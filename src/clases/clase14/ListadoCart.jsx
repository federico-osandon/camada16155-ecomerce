import React from 'react'
import { useAppContext } from '../../Context/AppContext'

const ListadoCart = () => {
    //traer del context
    const {product , precioTotal} = useAppContext() 
    return (
        <>
            {product.map(pro =>  <div> 
                                    <p>{ pro.item.name}</p>
                                    <p>{ pro.quantity}</p>
                                </div>
            )}
            {precioTotal()}
        </>
    )
}

export default ListadoCart
