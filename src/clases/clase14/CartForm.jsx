import {useState} from 'react'
import { getFirestore } from '../../service/getFirebase'

import firebase from "firebase";
import { useAppContext } from '../../Context/AppContext';

const CartForm = ({inputs, formData, setFormData, inatialState}) => {    

     //traer del context
     const {product , precioTotal, borrarListado} = useAppContext() 

     //manejo de los campos del formulario
     function handleChange(e) {
         setFormData(
             {
                 ...formData,
                 [e.target.name]: e.target.value
             }
         )
     }
     
 
     function handleSubmit(e){
         e.preventDefault()
         const newOrder={
             buyer: formData,
             itmes: product,
             date: firebase.firestore.Timestamp.fromDate(new Date()),
             total: precioTotal()
         }
         
 
 
         const db = getFirestore()
         const orders = db.collection('orders')
 
         //controlar si hay los productos que quiero agregar 
         orders.add(newOrder)
         .then(resp => alert(`la orden de compra es: ${resp.id}`))
         .catch(err => console.log(err))
         .finally(()=>{
             setFormData(inatialState)
             borrarListado()
         })
 
         // db.collection('items').doc('j4SIkIt0PlhJUs4UGnWB')
         // .update({
         //     stock: 3
         // })
         // .then(res => console.log(res))
         // .catch(err => console.log(err))
 
 
     }

    return (
        <>
             <form 
                onSubmit={handleSubmit}
                onChange={handleChange}
            >
                {inputs.map(inp => <input type={inp.type} placeholder={inp.placeholder} name={inp.name} value={inp.value} />)}

                <button >Terminar compra</button>
            </form>  
        </>
    )
}




export default CartForm
