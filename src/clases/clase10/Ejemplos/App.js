import React, { useState } from "react";
import "./style.css";
import Home from './Home';
import Cart from './Cart';
import AppContextProvider from './AppContext';
import CartContextProvider from './CartContext';


export default function App() {



  return (
    <AppContextProvider>
      <CartContextProvider>
        <Home/>
        <Cart/>
      </CartContextProvider>
    </AppContextProvider>
  );
}
