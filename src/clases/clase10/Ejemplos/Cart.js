import React from 'react';
import { useCartContext } from './CartContext';
import CartItem from './CartItem';
import './style.css';

export default function Cart() {
  const { cart, deleteCart } = useCartContext();

  return (
    <div>
      <h1>Cart!!</h1>

      {cart.length !== 0 && (
        <>
          <button onClick={deleteCart}>Limpiar carrito</button>
          {cart.map(product => (
            <CartItem key={product.id} product={product} />
          ))}
        </>
      )}
      {cart.length === 0 && <p>No hay productos en el carrito :(</p>}
    </div>
  );
}
