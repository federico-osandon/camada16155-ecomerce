console.log('sugart syntax')

// const condition = true 
// let result = ''
// if (condition) {
//     result = 'correct';
// }else{
//     result = 'incorrect';
// }
// console.log(`Este es el resultado ${result}`);



// const condition1= false
// console.log(`Este es el resultado ${condition1 ? 'correct': 'incorrect'}`);//condition1 && 'correct'

// let arr= ['b','c','d']
// let a= 'a';
// console.log([a, ...arr])

//propiedades dinamicas
let id = 'YEdad'

const Fede = { 
    foo: "bar",
    apellido: 'Osandón',
    [ "nom" + id]: 42,
    edad: 48
}
console.log(Fede);

let { foo , apellido, edad=43 } = Fede
console.log(edad);

// var { a: val } = { a : 2 }

// console.log(a)

