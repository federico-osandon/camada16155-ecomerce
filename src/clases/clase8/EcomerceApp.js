import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import NavBar from './components/NabVar/NavBar';
import ItemDetailContainer from './components/containers/ItemDetailContainer/ItemDetailContainer';
import ItemListContainer from './components/containers/ItemListContainer/ItemListContainer';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Contacto from './components/Contacto/Contacto';
//https://pokeapi.co/api/v2/pokemon
function EcomerceApp() {  

    return (
        <div className="App" >
            <Router>
                <NavBar />
                    <Switch>

                        <Route exact path='/'>
                            <ItemListContainer titulo={'hola'} />
                        </Route>

                        <Route exact path='/categoria/:category'>
                            <ItemListContainer titulo={'hola'} />
                        </Route>

                        <Route exact path="/contacto">
                            <Contacto />
                        </Route>    

                        <Route exact path='/detalle' component={ItemDetailContainer} />
                        
                        {/* <Cart /> */}
                    </Switch>
                {/* <Footer /> */}
                {/* <ItemCount /> */}
            </Router>
            </div>
    );
}

export default EcomerceApp;
