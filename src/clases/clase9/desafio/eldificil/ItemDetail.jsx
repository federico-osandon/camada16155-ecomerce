import { useState } from "react";
import {Link} from 'react-router-dom'
import ItemCount from "../../ItemCount/ItemCount"
import { useAppContext } from "../../../Context/AppContext"
import { Col, Row } from "react-bootstrap"


function ItemDetail( props ) {
    //Estado set
    const [cambiarBoton, setCambiarBoton] = useState(false)

    const { producto} = props 
    
    const {agregarAlCarrito} = useAppContext()
   
    const onAdd=(cant)=>{
        console.log(cant)  
        agregarAlCarrito(producto, cant)   
        setCambiarBoton(true)       
    }

    return (
        <>
            <Row>
                <label>Soy el detalle</label>
                <Col>                
                    <div className='card w-50'>
                        <div className="container">
                            <label>{producto.name}</label>
                        </div>
                        <div className="container">
                            <img  src={producto.url} className="w-25" />
                            <br/>
                            <label>{producto.descripcion}</label>
                        </div>
                        <div className="container">
                            <label>{producto.price}</label>
                        </div>
                    </div>
                </Col>
                <Col>
                    <ItemCount initial={1} stock={5} onAdd={onAdd} cambiarBoton={cambiarBoton} />
                    {
                        cambiarBoton && 
                            <Link to='/cart'>
                                <button className="btn btn-outline-primary btn-block" >terminar compra</button>
                            </Link>
                    }
                </Col>
            </Row>

        </>
    )
}

export default ItemDetail
