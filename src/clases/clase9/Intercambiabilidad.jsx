import { useState } from "react";


const InputCount= ()=> {
    return <button className="btn">Ir al Cart o Terminar compra</button>
}

const ButtonCount= ()=> {
    return <button className="btn">Agregar Al carrito</button>

}

const Intercambiabilidad = () => {

    const [inputType, setInputType ] = useState('button')

    const handleInter=()=>{
        setInputType('input')
    }
    
    return (
        <div onClick={handleInter}>
            <h2>Item Description</h2>
            {
                inputType === 'button' ? 
                    <ButtonCount />
                : 
                    <InputCount />
            }
            {/* <Count onConfirm={addToCart} maxQuantity={itemMax} /> */}
        </div>
    )
}

export default Intercambiabilidad
