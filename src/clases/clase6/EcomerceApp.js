import {useEffect} from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from './components/NabVar/NavBar';

//Promise((res, rej)=>{})
//res funcion cunado esta todo bien
//rej funcion cunado esta todo mal

const personas = [
    { id: 1, name: "juan", age: 37 },
    { id: 2, name: "carlos", age: 27 },
    { id: 3, name: "ana", age: 40 },
    { id: 4, name: "sofia", age: 55 },
    { id: 5, name: "victoria", age: 11 },
    { id: 6, name: "federico", age: 19 },
    { id: 7, name: "pepe", age: 2 }
  ];

function getPersonas(id) {
    if (id===undefined) {
        return personas
    }else{
        return personas.find( persona=> persona.id === id)
    }
}

let tarea = new Promise((resolve) => {
    //console.log("ejecutando promesa") 
    setTimeout(() => {
        resolve(getPersonas(6));        
    }, 3000);
    //reject("error 404 ");
});
//console.log(tarea);


function EcomerceApp() {  
    //estado
    useEffect(() => {
        tarea
        .then((resp)=> console.log(resp) )     //guardar en el estado
    }, [])

    // tarea
    // .then( (respuesta) => {
    //     //throw new Error("Error")
    //     console.log("respuesta: ", respuesta)
    //     return 1
    // })
    // .catch((err) => {
    // console.log("ourrio un error", err);
    // //return "todo ok";
    // })
    // .then((valor) => {
    // console.log("valor", valor);
    // });
    // console.log('llamada a api')

    //console.log()

    return (
        <div className="App" >
            <ul>
                { personas.map((persona)=>  <div key={persona.id} className='card w-50 mt-2'>
                    <div className="card-header">
                        {persona.name}
                    </div>
                    <div className="card-body">
                        {persona.age}
                    </div>
                </div>  )  }
            </ul>  
        </div>
    );
}

export default EcomerceApp;
