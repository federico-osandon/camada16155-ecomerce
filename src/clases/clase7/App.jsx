import { useState, useEffect } from "react";


export default function App() {
  const [pokes, setPokes] = useState([]);
  const pokemonID = 1;

  useEffect(() => {
    fetch(`https://pokeapi.co/api/v2/pokemon/`)
      .then((res) => res.json())
      .then((resp) => setPokes(resp.results));
  }, []);

  
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      {pokes.map((poke) => (
        <h2>{poke.name}</h2>
      ))}
    </div>
  );
}

// Aunque muchas veces se usan los términos Rest y Restful
// como sinónimos, no lo son. Rest (Represe
// ntational State Transfer), es un modelo de arquitec
// tura web basado en el protocolo HTTP para mejorar
// las comunicaciones cliente-servidor, mientras que Rest
// ful Web Service o Restful API son programas basados
// en REST. Su principal diferencia es que la API no neces
// ita ejecutar una red, sino que es posible hacerlo
// desde un mismo ordenador.

// Rest está diseñado para ser visible y simple,
//lo que significa que cada aspecto del servicio debe
//ser autodescriptivo siguiendo las normas HTTP.

// https://api.mercadolibre.com/sites/MLA/search (Mercado libre)
// (pokemon) https://pokeapi.co/
