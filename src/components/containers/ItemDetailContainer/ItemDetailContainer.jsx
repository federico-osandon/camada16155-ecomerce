import { useState, useEffect } from "react"
import { useParams } from "react-router-dom";
import {Spinner} from "react-bootstrap";
import { getFirestore } from "../../../service/getFirebase";

import { getProduc } from "../../../utils/promesas";
import ItemDetail from "./ItemDetail";

//codigo js

function ItemDetailContainer() {
    //codigo js
    const [ producto, setProducto ] = useState({})
    const [ categorias, setCategorias ] = useState([])
    const [loading, setloading] = useState(true)

    const { category } = useParams()
     
    
    useEffect(() => {
        const db = getFirestore()
        db.collection('items').doc('j4SIkIt0PlhJUs4UGnWB').get()
        .then(resp => {
            if(resp.exists){
                setProducto({id: resp.id, ...resp.data()})
            }
        } )
        .catch(err=>console.log(err))
        .finally(()=> setloading(false))
      
    }, [])

    useEffect(() => {
        const db = getFirestore()
        db.collection('categorias').get()
        .then(resp => {
            if(resp.size!==0){
                setCategorias( resp.docs.map(cat => ( {id: cat.id, ...cat.data()} )) )
                
            }
        } )
        .catch(err=>console.log(err))
        .finally(()=> setloading(false))
       
    }, [])

    console.log(producto)
    console.log(categorias)
    return (
        <>
            {loading ? 
                    <Spinner animation="grow" variant="info" />
                : 
                <div>
                    <ItemDetail producto={producto} categorias={categorias} />                   
                </div>
            }
            
        </>
    )
}

export default ItemDetailContainer
