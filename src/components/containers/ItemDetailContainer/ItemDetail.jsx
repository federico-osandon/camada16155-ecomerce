
import {Link} from 'react-router-dom'
import ItemCount from "../../ItemCount/ItemCount"
import { useAppContext } from "../../../Context/AppContext"
import { Col, Row } from "react-bootstrap"


function ItemDetail( { producto, categorias } ) {
    //Estado set
     
    const { addProduct } = useAppContext()
   
    const onAdd=(cant)=>{
        console.log(cant)  
        addProduct(producto, cant)        
    }

    return (
        <>
            { categorias.map(categoria => <label key={categoria.id}> {categoria.nombre} | </label> ) }
            <Row>
                <label>Soy el detalle</label>
                <Col>                
                    <div className='card w-50'>
                        <div className="container">
                            <label>{producto.name}</label>
                        </div>
                        <div className="container">
                            <img  src={producto.imagenID} className="w-25" alt="foto" />
                            <br/>
                            <label>{producto.descripcion}</label>
                        </div>
                        <div className="container">
                            <label>{producto.price}</label>
                        </div>
                    </div>
                </Col>
                <Col>
                    <ItemCount initial={1} stock={5} onAdd={onAdd} />                   
                </Col>
            </Row>

        </>
    )
}

export default ItemDetail
